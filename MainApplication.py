##################################################
# Main Application for Launching Lucy AI ChatBot #
# Verison 1.0.1 - Released 2024-04-24            #
# Author - Lawrence Lutton                       #
##################################################

from Default_Functions import *

WriteLog(Message='Starting Main Application', FuncName='Main', ErrorType='Info')

#Lucy()
GUI()
WriteLog(Message='Closing Main Application', FuncName='Main', ErrorType='Info')