# Lucy AI ChatBot 1.0.1.8 - 2024-04-24
## Change Log info
- x.x.x.y Y is the currently active build, once complete Y will be dropped and the version locked in.
### ChangeLog started in ver. 1.0.1.x
#### Current Dev build 1.0.1.8
- Fixed AI training
- Added Text to Speech
- Added Speech to Text
- Added Get_Weather (Still beta)
- Added Emotional Marker Bot
- Cleaned up Default Functions and MainApp scripts

# How to run the app
## Setup Guide in progress
- for psutil to work you will need to install it first
- pip3 install psutil
- Need to install chattbot lib - pip install chatterbot
- pip3 install chatterbot==1.0.4 pytz ---> This is the one that worked...
- Need to install pandas lib - pip install pandas
- pip3 install pandas
- need to install pip3 install gTTS
- install speech pip3 install PyAudio
- pip3 install --upgrade pip setuptools wheel
- /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
- pip3 install SpeechRecognition
- pip3 install requests
- pip3 install beautifulsoup4
## This is for use with most scripts for use as default functions
### Includes
- WriteLog
- CSV File operations
- System info
- ChatPot AI 
- Text to Speech
- Speech to Text
- Weather
- and more to come...